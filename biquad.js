// Port of
// http://www.earlevel.com/main/2012/11/26/biquad-c-source-code/


class BiquadFilter {
	// The constructor leaves the filter in a unusable (or at least useless) state.
	// Use lowpass/highpass/... to initialize (or to change frequency/Q/gain)
	constructor(f) {
		this.z1 = this.z2 = 0;
		this.a0 = this.a1 = this.a2 = 0;
		this.b1 = this.b2 = 0;
	}

	// Call this at each step (every 1/sampleRate sec)
	process(input) {
		let  z0 = input * this.a0 + this.z1;
		this.z1 = input * this.a1 + this.z2 - this.b1 * z0;
		this.z2 = input * this.a2           - this.b2 * z0;
		return z0;
	}

	lowpass(f, Q = 1) {
		const K = Math.tan(Math.PI * f / audio.sampleRate);
		let norm = 1 / (1 + K / Q + K * K);
		this.a0 = this.a2 = K * K * norm;
		this.a1 = 2 * this.a0;
		this.b1 = 2 * (K * K - 1) * norm;
		this.b2 = (1 - K / Q + K * K) * norm;
	}

	highpass(f, Q = 1) {
		const K = Math.tan(Math.PI * f / audio.sampleRate);
		let norm = 1 / (1 + K / Q + K * K);
		this.a0 = this.a2 = 1 * norm;
		this.a1 = -2 * a0;
		this.b1 = 2 * (K * K - 1) * norm;
		this.b2 = (1 - K / Q + K * K) * norm;
	}

	bandpass(f, Q = 1) {
		const K = Math.tan(Math.PI * f / audio.sampleRate);
		let norm = 1 / (1 + K / Q + K * K);
		this.a0 = K / Q * norm;
		this.a1 = 0;
		this.a2 = -a0;
		this.b1 = 2 * (K * K - 1) * norm;
		this.b2 = (1 - K / Q + K * K) * norm;
	}

	notch(f, Q = 1) {
		const K = Math.tan(Math.PI * f / audio.sampleRate);
		let norm = 1 / (1 + K / Q + K * K);
		this.a0 = this.a2 = (1 + K * K) * norm;
		this.a1 = this.b1 = 2 * (K * K - 1) * norm;
		this.b2 = (1 - K / Q + K * K) * norm;
	}

	peak(f, gain, Q = 1) {
		const K = Math.tan(Math.PI * f / audio.sampleRate);
		const V = Math.pow(10, Math.abs(gain) / 20);
		if (gain >= 0) {    // boost
			let norm = 1 / (1 + 1/Q * K + K * K);
			this.a0 = (1 + V/Q * K + K * K) * norm;
			this.a1 = this.b1 = 2 * (K * K - 1) * norm;
			this.a2 = (1 - V/Q * K + K * K) * norm;
			this.b2 = (1 - 1/Q * K + K * K) * norm;
		} else {    // cut
			let norm = 1 / (1 + V/Q * K + K * K);
			this.a0 = (1 + 1/Q * K + K * K) * norm;
			this.a1 = this.b1 = 2 * (K * K - 1) * norm;
			this.a2 = (1 - 1/Q * K + K * K) * norm;
			this.b2 = (1 - V/Q * K + K * K) * norm;
		}
	}

	lowshelf(f, gain) {
		const K = Math.tan(Math.PI * f / audio.sampleRate);
		const V = Math.pow(10, Math.abs(gain) / 20);
		if (gain >= 0) {    // boost
			let norm = 1 / (1 + Math.SQRT2 * K + K * K);
			this.a0 = (1 + Math.sqrt(2*V) * K + V * K * K) * norm;
			this.a1 = 2 * (V * K * K - 1) * norm;
			this.a2 = (1 - Math.sqrt(2*V) * K + V * K * K) * norm;
			this.b1 = 2 * (K * K - 1) * norm;
			this.b2 = (1 - Math.SQRT2 * K + K * K) * norm;
		} else {    // cut
			let norm = 1 / (1 + Math.sqrt(2*V) * K + V * K * K);
			this.a0 = (1 + Math.SQRT2 * K + K * K) * norm;
			this.a1 = 2 * (K * K - 1) * norm;
			this.a2 = (1 - Math.SQRT2 * K + K * K) * norm;
			this.b1 = 2 * (V * K * K - 1) * norm;
			this.b2 = (1 - Math.sqrt(2*V) * K + V * K * K) * norm;
		}
	}

	highshelf(f, gain) {
		const K = Math.tan(Math.PI * f / audio.sampleRate);
		const V = Math.pow(10, Math.abs(gain) / 20);
		if (gain >= 0) {    // boost
			let norm = 1 / (1 + Math.SQRT2 * K + K * K);
			this.a0 = (V + Math.sqrt(2*V) * K + K * K) * norm;
			this.a1 = 2 * (K * K - V) * norm;
			this.a2 = (V - Math.sqrt(2*V) * K + K * K) * norm;
			this.b1 = 2 * (K * K - 1) * norm;
			this.b2 = (1 - Math.SQRT2 * K + K * K) * norm;
		} else {    // cut
			let norm = 1 / (V + Math.sqrt(2*V) * K + K * K);
			this.a0 = (1 + Math.SQRT2 * K + K * K) * norm;
			this.a1 = 2 * (K * K - 1) * norm;
			this.a2 = (1 - Math.SQRT2 * K + K * K) * norm;
			this.b1 = 2 * (K * K - V) * norm;
			this.b2 = (V - Math.sqrt(2*V) * K + K * K) * norm;
		}
	}
}
