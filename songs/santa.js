let N = 40;

function output(t, U, V) {

  if (!SANTA) {
    audio.T0 = t;
    return [0,0];
  }

  t = 6 + 2 * sin(t/2);

  let [l,r] = [santa(0, t), santa(1, t)];
  return [round(l*N)/N,
          round(r*N)/N];
}
