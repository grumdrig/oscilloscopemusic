
// Current iteration:
// https://dev.opera.com/articles/drum-sounds-webaudio/

// Great kick!:
// https://sonoport.github.io/synthesising-sounds-webaudio.html

// Hihat:
// http://joesul.li/van/synthesizing-hi-hats/

let F = 1800;
let lpl = new BiquadFilter();
lpl.lowpass(F);
let lpr = new BiquadFilter();
lpr.lowpass(F);

function output(t, U, V) {
  if ((t -= 0.25) < 0) return [0,0];  // Give everything time to spin up

  let BPM = 120;
  let BEAT_DURATION = 60 / BPM;
  let TIME_SIGNATURE = 4;

  let beat = (t / BEAT_DURATION) >> 0;
  let beatOffset = t - (beat * BEAT_DURATION);
  let beatFade = 1 - beatOffset / BEAT_DURATION;
  beatFade *= beatFade;
  let measure = (beat / TIME_SIGNATURE) >> 0;
  let count = beat % TIME_SIGNATURE;


  let r = [0,0];

  function circle(vol, phase) {
    r[0] += vol * sin(TAU * phase);
    r[1] += vol * cos(TAU * phase);
  }

  function square(vol, phase) {
    r[0] += vol * max(-1, min(1, 2 * sin(TAU * phase)));
    r[1] += vol * max(-1, min(1, 2 * cos(TAU * phase)));
  }

  function rando(size) {
    r[0] += size * rand();
    r[1] += size * rand();
  }

  let A = 0.001;
  let attack = min(1, beatOffset / A);

  if ((beat & 1) === 0) {
    // kick
    let env = 0.8 * Math.pow(0.00001, beatOffset);
    let K = 80;
    OSC[0].frequency = K * env;
    OSC[1].frequency = K * 1.5 * env;
    square(env, OSC[0].phase);
    circle(env, OSC[1].phase);
  } else if (1) {
    // snare
    // tonal noise
    OSC[0].frequency = 700 * Math.pow(.9, beatOffset);
    let s = 0.25 * Math.pow(.0000001, beatOffset);
    let [left, right] = [
      sin(3982.394 + 57923.834 * OSC[0].cycle) % 1,
      sin(8743.859 + 65172.598 * OSC[0].cycle) % 1];
    r[0] += s * left;
    r[1] += s * right;

    // white noise
    let v = 0.8 * Math.pow(.00001, beatOffset);
    r[0] += rand() * v;
    r[1] += rand() * v;

    r[0] = lpl.process(r[0]);
    r[1] = lpr.process(r[1]);
  }

  OSC[0].step();
  OSC[1].step();

  return [r[0] * attack,
          r[1] * attack];
}


function rand() {
  return Math.random() * 2 - 1;
}

