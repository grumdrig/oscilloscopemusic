function rotate(u, v, a) {
  let c = cos(a);
  let s = sin(a);
  return [c * u + s * v,
          c * v - s * u];
}

let BPM = 140;
let BEAT_DURATION = 60 / BPM;
let TIME_SIGNATURE = 4;

OSC[0].frequency = BPM / 60;

function output(t,U,V) {

  OSC[0].step();

  let beat = OSC[0].cycle;
  let beatOffset = OSC[0].offset();
  let beatFade = 1 - OSC[0].phase;
  let measure = (beat / TIME_SIGNATURE) >> 0;
  let count = beat % TIME_SIGNATURE;

  let f = [200, 300, 100, 240][count];

  OSC[1].frequency = f*U;
  OSC[1].step();

  ctx.fillStyle = `rgba(0,0,20,${V})`;
  ctx.strokeStyle = `rgba(${(t*1000%255)<<0},0,0,1)`;

  let p = 2 * OSC[1].phase - 1; // Hz
  let q = 2 * ((OSC[1].phase + 0.25) % 1) - 1;

  return rotate(beatFade * p,
                beatFade * q,
                t*.111);
}