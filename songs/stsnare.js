// Snare from Synthesizer Techniques
// https://books.google.com/books?id=IQrOFoWL0T4C&pg=PA14&lpg=PA14&dq=simulating+a+snare+drum&source=bl&ots=QBFM4WLoME&sig=ThQ1P8--qxFgxOQ8U8R5f0M-s4g&hl=en&sa=X&ved=0ahUKEwjMuvjI34_YAhUWwWMKHUBSBbo4ChDoAQg1MAM#v=onepage&q=simulating%20a%20snare%20drum&f=false

// initial crack of stick to head contact
// VCO 2: quick square wave sweep down a few octaves
// ASSR 2: controls this pitch but also the envelope
// fast enough to obscure the perception of falling pitch
// perceived pitch center around 600-800Hz

// metal snare decay
// filtered white noise
// lowpass ok, but bandpass will give more personality
// slower attack than crack
// maybe drop the filter lower toward end but dont overdo

// shell resonance
// 400Hz sine wave
// falls semitone or less
// same adsr (as snare?)

// METAL SNARES [NOISE] ---------> [VCF fc Q=low] -> [VCA] ---+--> mix
// SHELL RESNCE [VCO1 sine f] ---> [            ]      ^      |
//                 ^                     ^             |      |
//                 |        -------------+-------------|      |
//                 +--------+                                 |
//               [ADSR1] ---|          [VCO2 f]----->[VCA]----|

class Oscillator {
	constructor() {
		this.phase = 0;
		this.cycle = 0;
	}

	step(frequency) {
		this.phase += frequency / audio.sampleRate;
		if (this.phase >= 1) {
			this.cycle += this.phase << 0;
			this.phase %= 1;
		}
	}
}

function createAdshr(a, d, s, h, r) {
	return function(t) {
		if (t <= a) return t / a;
		t -= a;
		if (t <= d) return (t/d) * s + (1 - t/d);
		t -= d;
		if (t < h) return s;
		t -= h;
		if (t < r) return s * (1 - t/d);
		return 0;
	}
}

let vcoCrack = new Oscillator();
let vcoShell = new Oscillator();

let envCrack = createAdshr(0.01, 0.01, 0.8, 0.1, 0.2);
let envShell = createAdshr(0.1,  0.01, 0.8, 0.2, 0.3);

function square(x) { return Math.round(x) * 2 - 1 }
function sine(x) { return Math.sin(x * Math.PI * 2) }

function output(t) {
	t %= 1;

	let r = 0;

	r += envCrack(t) * square(vcoCrack.phase);

	r += envShell(t) * (Math.random() * 2 - 1);

	r += envShell(t) * sine(vcoShell.phase);

	vcoCrack.step(700);
	vcoShell.step(400);

	return [r, r];
}

