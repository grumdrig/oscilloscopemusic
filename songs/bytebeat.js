function output(t) {
  t = (t * audio.sampleRate) << 0;

  let u = ((t >> 10) & 42) * t;

  let v = (((t<<1)^((t<<1)+(t>>7)&t>>12))|t>>(4-(1^7&(t>>19)))|t>>7);

  return [
    (u % 511) / 255 - 1,
    (v & 511) / 255 - 1
  ];
}
