
function r(n=1) {
  return (Math.random()*2-1) * Math.pow(.8, n)
}

function output(t) {
  let P = 2 * (t) + 1;
  return [r(P), r(P)];
}
