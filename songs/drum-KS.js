let BPM = 120;
let BEAT_DURATION = 60 / BPM;
let TIME_SIGNATURE = 4;

// Karplus-Strong used to simulate a snare drum
// https://ccrma.stanford.edu/~sdill/220A-project/drums.html#ks

class Ks {
  constructor(b, p, damping = 1) {
    this.L = p * audio.sampleRate << 0;
    this.buffers = [0,0].map(_ => new Float32Array(this.L));
    this.pos = 0;
    this.b = b;
    this.damping = damping;
  }

  generate(n) {
    if ((this.pos += 1) >= this.L) this.pos = 0;
    return this.buffers.map(buf => {
      let v = (n < this.L) ? Math.random() :
        this.damping * (buf[this.pos] + buf[(this.pos + 1) % this.L]) / 2;
      if (Math.random() > this.b) v = -v;
      buf[this.pos] = v;;
      return v;
    });
  }
}

let snare = new Ks(0.5, 1000/audio.sampleRate, 0.997);

function output(t,U,V) {
  let beat = (t / BEAT_DURATION) >> 0;
  let beatOffset = t - (beat * BEAT_DURATION);
  let beatFade = 1 - beatOffset / BEAT_DURATION;
  let measure = (beat / TIME_SIGNATURE) >> 0;
  let count = beat % TIME_SIGNATURE;

  return snare.generate(beatOffset * audio.sampleRate << 0);
}
