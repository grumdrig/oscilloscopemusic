
function output(t) {
  let r = [0,0];
  let F = 200;
  let S = .1;
  let C = analysers[0].frequencyBinCount;
  let f0 = audio.sampleRate / C;
  let N = 4;
 for (let i = 1; i <= 128; i += 1) {
    let f = i * f0;
    r[0] += S * (1+Math.sin(f * TAU / N)) * Math.sin(t * TAU * f * F);
    r[1] += S * (1+Math.cos(f * TAU / N)) * Math.cos(t * TAU * f * F);
  }

  return r;
}