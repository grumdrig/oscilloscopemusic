const r = Math.random;
const TAU = Math.PI * 2;

let N = 51;
let tones = new Array(N).fill().map((v,i) => [0]);

function stepOsc(osc, frequency) {
	osc[0] += frequency / audio.sampleRate;
	if (osc[0] >= 1) {
		//osc[1] = (osc[1] || 0) + (this.phase << 0);
		osc[0] %= 1;
	}
}

function limit(wave) {
	return [
		min(1, max(-1, wave[0])),
		min(1, max(-1, wave[1]))];
}

function square(phase) { limit(scale(sine(phase), Math.SQRT2)) }

function sine(phase) {
	return [
		Math.sin(phase * TAU),
		Math.cos(phase * TAU)];
}

function add(r, w) {
	r[0] += w[0];
	r[1] += w[1];
}

function scale(v, s) { return [v[0] * s, v[1] * s] }

let F = 200;
function output(t) {
	let result = [0,0];
	for (let i = 0; i < N; ++i) {
		let tone = tones[i];
		let n = i * 2 + 1;
		stepOsc(tone, F * n + .001 * n * (n - 1));
		add(result, scale(sine(tone[0]), .5/(i+1)));
	}
	return limit(result);
}