
class Oscillator {
	constructor(p) {
		this.phase = 0;
		this.cycle = 0;
                this.p = p;
	}

	step(frequency) {
		this.phase += frequency / audio.sampleRate;
		if (this.phase >= 1) {
			this.cycle += this.phase << 0;
			this.phase %= 1;
		}
	}
}

let o1 = new Oscillator();
let o2 = new Oscillator();

let filter = new StereoBiquadFilter();
let band = 3000;
filter.left.lowpass(band);
filter.right.lowpass(band);

function random() { return Math.random() * 2 - 1 }

let oo = [1,2,3,4,5,6,7].map(p => {
  //p = p * 2 + 1;
  let o = new Oscillator();
  o.phase = 1 / p;
  o.p = p + random() * 0.001;
  return o;
});



function output(t) {
  t = t % 1 + 0.1;
  //ctx.fillStyle = `rgba(0, 0, 0, ${1/t})`;
  let F = 100 / t;
  let r = [0,0];
  for (let o of oo) {
    o.step(o.p * F);
    r[0] += sin(2 * PI * o.phase) / o.p / 2;
    r[1] += cos(2 * PI * o.phase) / o.p / 2;
  }
  let b = (t + 0.5) % 1 + 0.1;
  let x = 1 * Math.pow(0.25, b * 5);
  r[0] += x * random();
  r[1] += x * random();
  filter.process(r);
  return r;
}
