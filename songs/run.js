// "Run" by Awolnation
const r = Math.random;
const TAU = Math.PI * 2;


let SONG = [  // note, duration in eighths // count
   0, // 1
   0,
   0, // 2
  12,
  15, //3
   7,
  10, //4
  12,
  14, // 1
  14,
  14, // 2
   0,
  10, // 3
  10,
  12, // 4
  12,
  ];
SONG = SONG.concat(SONG).concat(SONG).concat([
   0, // 1
   0,
   0, // 2
  12,
  15, //3
   7,
   8, //4
  10,
  11, // 1
  11,
  11, // 2
   0,
  12, // 3
  12,
  14, // 4
  14,
  ]);


let P = Math.pow(2, 1/12);

let F = 110 / P;  // Key of Ab I think

let BPM = 268;
let SPB = 60 / BPM;

let N = 21;
let tones = new Array(N).fill().map((v,i) => [0]);

function stepOsc(osc, frequency) {
	osc[0] += frequency / audio.sampleRate;
	if (osc[0] >= 1) {
		//osc[1] = (osc[1] || 0) + (this.phase << 0);
		osc[0] %= 1;
	}
}

function limit(wave) {
	return [
		min(1, max(-1, wave[0])),
		min(1, max(-1, wave[1]))];
}

function square(phase) { limit(scale(sine(phase), Math.SQRT2)) }

function sine(phase) {
	return [
		Math.sin(phase * TAU),
		Math.cos(phase * TAU)];
}

function add(r, w) {
	r[0] += w[0];
	r[1] += w[1];
}

function scale(v, s) { return [v[0] * s, v[1] * s] }


function output(t) {
	let beat = (t / SPB) << 0;
	let offset = (t / SPB) - beat;
	offset = min(1, offset * 4);
	let inote = SONG[beat % SONG.length];
	let ilast = SONG[(beat + SONG.length - 1) % SONG.length];
	let note = F * Math.pow(P, inote);
	let last = F * Math.pow(P, ilast);
	note = offset * note + (1 - offset) * last;

	let result = [0,0];
	for (let i = 0; i < N; ++i) {
		let tone = tones[i];
		let n = i * 2 + 1;
		stepOsc(tone, note * n + .0004 * (inote - 3) * n * (n - 1));
		add(result, scale(sine(tone[0]), .25/(i+1)));
	}
	return limit(result);
}